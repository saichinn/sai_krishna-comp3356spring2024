#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <math.h>

int approximate_alphabet(char *str) {
    int alphabet_size = 0;
    int lowercase = 0, uppercase = 0, digits = 0, symbols = 0;

    while (*str != '\0') {
        if (islower(*str)) lowercase = 1;
        else if (isupper(*str)) uppercase = 1;
        else if (isdigit(*str)) digits = 1;
        else symbols = 1;
        str++;
    }

    if (lowercase) alphabet_size += 26;
    if (uppercase) alphabet_size += 26;
    if (digits) alphabet_size += 10;
    if (symbols) alphabet_size += 32;

    return alphabet_size;
}

double information_content(int alphabet_size, size_t length) {
    return length * log2(alphabet_size);
}

int main() {
    char str[101];
    printf("Enter the string: ");
    fgets(str, sizeof(str), stdin);
    str[strcspn(str, "\n")] = '\0'; 

    int alphabet = approximate_alphabet(str);
    size_t length = strlen(str);

    printf("Approximate alphabet: %d\n", alphabet);
    printf("Length: %zu\n", length);
    printf("Information Content: %.2f\n", information_content(alphabet, length));

    return 0;
}
