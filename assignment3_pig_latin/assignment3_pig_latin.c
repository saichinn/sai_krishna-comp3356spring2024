#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define MAX_WORD_LENGTH 100
#define MAX_LINE_LENGTH 1000

int is_vowel(char c) {
    return (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u' || c == 'y');
}

void encode_word(char *word) {
    char encoded[MAX_WORD_LENGTH + 3]; 
    int len = strlen(word);
    int i, j;

    if (is_vowel(word[0]) || (word[0] == 'y' && !is_vowel(word[1]))) {
        strcpy(encoded, word);
        strcat(encoded, "yay");
    } else {
        for (i = 1; i < len; i++) {
            if (is_vowel(word[i])) {
                break;
            }
        }
        strcpy(encoded, &word[i]);
        strncat(encoded, word, i);
        strcat(encoded, "ay");
    }

    strcpy(word, encoded);
}

int main() {
    char line[MAX_LINE_LENGTH + 1];
    char word[MAX_WORD_LENGTH + 1];
    char ch;
    int word_index = 0;
    int i;

    while (fgets(line, sizeof(line), stdin)) {
        for (i = 0; line[i] != '\0'; i++) {
            ch = line[i];
            if (isalpha(ch) || ch == '\'' || ch == '-') {
                word[word_index++] = ch;
            } else {
                if (word_index > 0) {
                    word[word_index] = '\0';
                    encode_word(word);
                    printf("%s", word);
                    word_index = 0;
                }
                printf("%c", ch);
            }
        }
        if (word_index > 0) {
            word[word_index] = '\0';
            encode_word(word);
            printf("%s", word);
            word_index = 0;
        }
    }

    return 0;
}
