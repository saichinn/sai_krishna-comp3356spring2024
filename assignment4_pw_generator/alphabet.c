#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "alphabet.h"

void parse_alphabet(int argc, char *argv[], char alphabet[], int flags_luds[]) {
    memset(alphabet, 0, 128); // Initialize alphabet array with size 128 (for ASCII characters)

    // Iterate through command line arguments starting from index 3
    for (int i = 3; i < argc; ++i) {
        if (argv[i][0] == '-') {
            for (int j = 1; argv[i][j] != '\0'; ++j) {
                switch (argv[i][j]) {
                    case 'l':
                        flags_luds[0] = 1;
                        break;
                    case 'u':
                        flags_luds[1] = 1;
                        break;
                    case 'd':
                        flags_luds[2] = 1;
                        break;
                    case 's':
                        flags_luds[3] = 1;
                        break;
                    default:
                        fprintf(stderr, "Warning: Unknown flag '%c' ignored.\n", argv[i][j]);
                        break;
                }
            }
        } else {
            // Add characters from alphabet argument
            for (int j = 0; argv[i][j] != '\0'; ++j) {
                if (is_valid_character(argv[i][j])) {
                    alphabet[(unsigned char)argv[i][j]] = 1;
                } else {
                    fprintf(stderr, "Warning: Invalid character '%c' skipped.\n", argv[i][j]);
                }
            }
        }
    }

    // Add characters based on -luds flags if no explicit alphabet is provided
    if (!alphabet[0] && !alphabet['A']) {
        for (int i = 'a'; i <= 'z'; ++i) {
            if (flags_luds[0]) alphabet[i] = 1;
        }
        for (int i = 'A'; i <= 'Z'; ++i) {
            if (flags_luds[1]) alphabet[i] = 1;
        }
        for (int i = '0'; i <= '9'; ++i) {
            if (flags_luds[2]) alphabet[i] = 1;
        }
        if (flags_luds[3]) {
            const char symbols[] = "!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~";
            for (int i = 0; symbols[i] != '\0'; ++i) {
                alphabet[(unsigned char)symbols[i]] = 1;
            }
        }
    }
}

int is_valid_character(char ch) {
    return isgraph((unsigned char)ch); // Check if character is a graphical ASCII character
}
