#ifndef ALPHABET_H
#define ALPHABET_H

void parse_alphabet(int argc, char *argv[], char alphabet[], int flags_luds[]);
int is_valid_character(char ch);

#endif /* ALPHABET_H */
