
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/home/kali/Documents/passwd/assignment4_pw_generator/alphabet.c" "CMakeFiles/pwgen.dir/alphabet.c.o" "gcc" "CMakeFiles/pwgen.dir/alphabet.c.o.d"
  "/home/kali/Documents/passwd/assignment4_pw_generator/information_content.c" "CMakeFiles/pwgen.dir/information_content.c.o" "gcc" "CMakeFiles/pwgen.dir/information_content.c.o.d"
  "/home/kali/Documents/passwd/assignment4_pw_generator/main.c" "CMakeFiles/pwgen.dir/main.c.o" "gcc" "CMakeFiles/pwgen.dir/main.c.o.d"
  "/home/kali/Documents/passwd/assignment4_pw_generator/pw_generator.c" "CMakeFiles/pwgen.dir/pw_generator.c.o" "gcc" "CMakeFiles/pwgen.dir/pw_generator.c.o.d"
  )

# Targets to which this target links which contain Fortran sources.
set(CMAKE_Fortran_TARGET_LINKED_INFO_FILES
  )

# Targets to which this target links which contain Fortran sources.
set(CMAKE_Fortran_TARGET_FORWARD_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
