#include <math.h>
#include <string.h>
#include "information_content.h"

double calculate_information_content(const char password[], int alphabet_size) {
    int len = strlen(password);
    double info_content = len * log2(alphabet_size);

    // Count occurrences of each character in the password
    int char_count[128] = {0};
    for (int i = 0; i < len; ++i) {
        char_count[(unsigned char)password[i]]++;
    }

    // Calculate Shannon entropy
    double entropy = 0.0;
    for (int i = 0; i < 128; ++i) {
        if (char_count[i] > 0) {
            double probability = (double)char_count[i] / len;
            entropy -= probability * log2(probability);
        }
    }

    return info_content + entropy;
}
