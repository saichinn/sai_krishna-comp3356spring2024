#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "alphabet.h"
#include "pw_generator.h"
#include "information_content.h"

int main(int argc, char *argv[]) {
    if (argc < 3) {
        fprintf(stderr, "Usage: %s length quantity [-luds] [alphabet]\n", argv[0]);
        return 1;
    }

    int length = atoi(argv[1]);
    int quantity = atoi(argv[2]);

    if (length <= 0 || quantity <= 0) {
        fprintf(stderr, "Error: length and quantity must be positive integers.\n");
        return 1;
    }

    // Parse command line arguments to get alphabet
    char alphabet[128] = {0}; // Initialize alphabet array with size 128 (for ASCII characters)
    int flags_luds[4] = {0}; // l, u, d, s flags

    parse_alphabet(argc, argv, alphabet, flags_luds);

    // Calculate the size of the alphabet
    int alphabet_size = 0;
    for (int i = 0; i < 128; ++i) {
        if (alphabet[i]) {
            alphabet_size++;
        }
    }

    // Print the used alphabet
    printf("Using alphabet: ");
    for (int i = 0; i < 128; ++i) {
        if (alphabet[i]) {
            putchar(i);
        }
    }
    printf("\n\n");

    // Generate and print passwords
    for (int i = 0; i < quantity; ++i) {
        char password[length + 1]; // +1 for null terminator
        generate_password(length, alphabet, flags_luds, password);
        double info_content = calculate_information_content(password, alphabet_size);
        
        printf("Password %d:\n", i + 1);
        printf("Password: %s\n", password);
        printf("Information content: %.2f bits\n\n", info_content);
    }

    return 0;
}
