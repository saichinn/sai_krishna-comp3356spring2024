#include <stdlib.h>
#include "pw_generator.h"
#include "alphabet.h"

void generate_password(int length, char alphabet[], int flags_luds[], char password[]) {
    int alphabet_size = 0;
    char valid_chars[128]; // Assuming ASCII characters

    // Build valid_chars array based on alphabet and flags_luds
    for (int i = 0; i < 128; ++i) {
        if (alphabet[i]) {
            valid_chars[alphabet_size++] = (char)i;
        }
    }

    // Generate password using valid_chars
    for (int i = 0; i < length; ++i) {
        int random_index = rand() % alphabet_size;
        password[i] = valid_chars[random_index];
    }
    password[length] = '\0'; // Null-terminate the password
}
