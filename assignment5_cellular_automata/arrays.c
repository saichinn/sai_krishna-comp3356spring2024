#include <stdlib.h>
#include "arrays.h"

bool **malloc_2d(int width, int height) {
    bool **array = (bool **)malloc(height * sizeof(bool *));
    for (int i = 0; i < height; i++) {
        array[i] = (bool *)malloc(width * sizeof(bool));
    }
    return array;
}

void free_2d(bool **array, int height) {
    for (int i = 0; i < height; i++) {
        free(array[i]);
    }
    free(array);
}

bool *malloc_row_major(int width, int height) {
    return (bool *)malloc(width * height * sizeof(bool));
}

void free_row_major(bool *array) {
    free(array);
}

int row_major(int row, int col, int width) {
    return row * width + col;
}
