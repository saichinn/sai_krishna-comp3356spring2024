#include <stdio.h>
#include <stdlib.h>
#include "picture.h"
#include "arrays.h"
#include "rules.h"

void process_command_line(int argc, char *argv[], int *width, int *height) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s width height\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    *width = atoi(argv[1]);
    *height = atoi(argv[2]);
}

int main(int argc, char *argv[]) {
    int width, height;
    process_command_line(argc, argv, &width, &height);

    printf("Drawing %d x %d\n", width, height);

    rule_18_picture(width, height);
    rule_57_picture(width, height);
    rule_60_picture(width, height);
    rule_73_picture(width, height);

    return 0;
}
