#include <stdio.h>
#include <stdlib.h>
#include "picture.h"
#include "arrays.h"
#include "rules.h"

void initialize_first_row(bool *array, int width) {
    for (int i = 0; i < width; i++) {
        array[i] = false;
    }
    array[width / 2] = true;
}

void print_2d(bool **array, int width, int height) {
    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            printf(array[i][j] ? "X" : ".");
        }
        printf("\n");
    }
}

void print_row_major(bool *array, int width, int height) {
    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            printf(array[row_major(i, j, width)] ? "X" : ".");
        }
        printf("\n");
    }
}

void rule_18_picture(int width, int height) {
    bool **array = malloc_2d(width, height);
    initialize_first_row(array[0], width);

    for (int i = 1; i < height; i++) {
        for (int j = 0; j < width; j++) {
            bool left = (j == 0) ? false : array[i-1][j-1];
            bool center = array[i-1][j];
            bool right = (j == width-1) ? false : array[i-1][j+1];
            array[i][j] = rule_18(left, center, right);
        }
    }

    printf("Rule 18\n");
    print_2d(array, width, height);
    free_2d(array, height);
}

void rule_57_picture(int width, int height) {
    bool **array = malloc_2d(width, height);
    initialize_first_row(array[0], width);

    for (int i = 1; i < height; i++) {
        for (int j = 0; j < width; j++) {
            bool left = (j == 0) ? false : array[i-1][j-1];
            bool center = array[i-1][j];
            bool right = (j == width-1) ? false : array[i-1][j+1];
            array[i][j] = rule_57(left, center, right);
        }
    }

    printf("Rule 57\n");
    print_2d(array, width, height);
    free_2d(array, height);
}

void rule_60_picture(int width, int height) {
    bool *array = malloc_row_major(width, height);
    initialize_first_row(array, width);

    for (int i = 1; i < height; i++) {
        for (int j = 0; j < width; j++) {
            bool left = (j == 0) ? false : array[row_major(i-1, j-1, width)];
            bool center = array[row_major(i-1, j, width)];
            bool right = (j == width-1) ? false : array[row_major(i-1, j+1, width)];
            array[row_major(i, j, width)] = rule_60(left, center, right);
        }
    }

    printf("Rule 60\n");
    print_row_major(array, width, height);
    free_row_major(array);
}

void rule_73_picture(int width, int height) {
    bool *array = malloc_row_major(width, height);
    initialize_first_row(array, width);

    for (int i = 1; i < height; i++) {
        for (int j = 0; j < width; j++) {
            bool left = (j == 0) ? false : array[row_major(i-1, j-1, width)];
            bool center = array[row_major(i-1, j, width)];
            bool right = (j == width-1) ? false : array[row_major(i-1, j+1, width)];
            array[row_major(i, j, width)] = rule_73(left, center, right);
        }
    }

    printf("Rule 73\n");
    print_row_major(array, width, height);
    free_row_major(array);
}
