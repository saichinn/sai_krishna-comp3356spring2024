#include <stdio.h>
#include <assert.h>
#include "rules.h"

void test_rule_18(void) {
    assert(rule_18(false, false, false) == false);
    assert(rule_18(false, false, true) == true);
    assert(rule_18(false, true, false) == true);
    assert(rule_18(false, true, true) == false);
    assert(rule_18(true, false, false) == true);
    assert(rule_18(true, false, true) == false);
    assert(rule_18(true, true, false) == false);
    assert(rule_18(true, true, true) == false);
    printf("All Rule 18 tests passed!\n");
}

void test_rule_57(void) {
    assert(rule_57(false, false, false) == false);
    assert(rule_57(false, false, true) == true);
    assert(rule_57(false, true, false) == true);
    assert(rule_57(false, true, true) == true);
    assert(rule_57(true, false, false) == false);
    assert(rule_57(true, false, true) == true);
    assert(rule_57(true, true, false) == true);
    assert(rule_57(true, true, true) == false);
    printf("All Rule 57 tests passed!\n");
}

void test_rule_60(void) {
    assert(rule_60(false, false, false) == false);
    assert(rule_60(false, false, true) == false);
    assert(rule_60(false, true, false) == true);
    assert(rule_60(false, true, true) == true);
    assert(rule_60(true, false, false) == true);
    assert(rule_60(true, false, true) == false);
    assert(rule_60(true, true, false) == false);
    assert(rule_60(true, true, true) == false);
    printf("All Rule 60 tests passed!\n");
}

void test_rule_73(void) {
    assert(rule_73(false, false, false) == false);
    assert(rule_73(false, false, true) == true);
    assert(rule_73(false, true, false) == true);
    assert(rule_73(false, true, true) == false);
    assert(rule_73(true, false, false) == true);
    assert(rule_73(true, false, true) == false);
    assert(rule_73(true, true, false) == false);
    assert(rule_73(true, true, true) == false);
    printf("All Rule 73 tests passed!\n");
}
