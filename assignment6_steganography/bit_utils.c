#include "bit_utils.h"

unsigned char get_bit(unsigned char byte, int bit) {
    return (byte >> bit) & 1;
}

unsigned char set_bit(unsigned char byte, int bit) {
    return byte | (1 << bit);
}

unsigned char clear_bit(unsigned char byte, int bit) {
    return byte & ~(1 << bit);
}

unsigned char toggle_bit(unsigned char byte, int bit) {
    return byte ^ (1 << bit);
}
