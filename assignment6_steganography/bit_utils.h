#ifndef BIT_UTILS_H
#define BIT_UTILS_H

unsigned char get_bit(unsigned char byte, int bit);
unsigned char set_bit(unsigned char byte, int bit);
unsigned char clear_bit(unsigned char byte, int bit);
unsigned char toggle_bit(unsigned char byte, int bit);

#endif // BIT_UTILS_H
