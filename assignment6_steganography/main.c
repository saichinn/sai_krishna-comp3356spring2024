#include <stdio.h>
#include <string.h>
#include "ppm.h"
#include "steganography.h"

void print_usage() {
    printf("Usage:\n");
    printf("  Encode: ./steganography encode input.ppm payload.ext output.ppm\n");
    printf("  Decode: ./steganography decode input.ppm output.ext\n");
}

int main(int argc, char *argv[]) {
    if (argc != 5 && argc != 4) {
        print_usage();
        return 1;
    }

    if (strcmp(argv[1], "encode") == 0) {
        if (argc != 5) {
            fprintf(stderr, "Error: Insufficient arguments for encoding.\n");
            return 1;
        }
        if (strcmp(argv[2], argv[4]) == 0) {
            fprintf(stderr, "Error: Input and output filenames must be unique.\n");
            return 1;
        }
        return encode(argv[2], argv[3], argv[4]);
    } else if (strcmp(argv[1], "decode") == 0) {
        if (argc != 4) {
            fprintf(stderr, "Error: Insufficient arguments for decoding.\n");
            return 1;
        }
        return decode(argv[2], argv[3]);
    } else {
        fprintf(stderr, "Error: Invalid mode specified.\n");
        print_usage();
        return 1;
    }
}
