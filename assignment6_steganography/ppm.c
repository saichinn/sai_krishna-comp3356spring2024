#include <stdio.h>
#include <stdlib.h>
#include <string.h> 
#include "ppm.h"

PPMImage *read_ppm(const char *filename) {
    FILE *fp = fopen(filename, "r");
    if (!fp) {
        perror("Error opening file");
        return NULL;
    }

    PPMImage *image = malloc(sizeof(PPMImage));
    if (!image) {
        perror("Error allocating memory");
        fclose(fp);
        return NULL;
    }

    char format[3];
    int max_val;
    if (fscanf(fp, "%2s\n%d %d\n%d\n", format, &image->width, &image->height, &max_val) != 4 ||
        strcmp(format, "P3") != 0 || max_val != 255) {
        fprintf(stderr, "Invalid PPM format\n");
        free(image);
        fclose(fp);
        return NULL;
    }

    printf("PPM Format: %s\n", format);
    printf("Width: %d, Height: %d\n", image->width, image->height);
    printf("Max Value: %d\n", max_val);

    int size = image->width * image->height * 3;
    image->data = malloc(size);
    if (!image->data) {
        perror("Error allocating memory");
        free(image);
        fclose(fp);
        return NULL;
    }

    for (int i = 0; i < size; i++) {
        if (fscanf(fp, "%hhu", &image->data[i]) != 1) {
            fprintf(stderr, "Error reading pixel data\n");
            free(image->data);
            free(image);
            fclose(fp);
            return NULL;
        }
    }

    fclose(fp);
    return image;
}


int write_ppm(const char *filename, PPMImage *image) {
    FILE *fp = fopen(filename, "w");
    if (!fp) {
        perror("Error opening file");
        return 1;
    }

    fprintf(fp, "P3\n%d %d\n255\n", image->width, image->height);
    int size = image->width * image->height * 3;
    for (int i = 0; i < size; i++) {
        fprintf(fp, "%d\n", image->data[i]);
    }

    fclose(fp);
    return 0;
}

void free_ppm(PPMImage *image) {
    if (image) {
        free(image->data);
        free(image);
    }
}
