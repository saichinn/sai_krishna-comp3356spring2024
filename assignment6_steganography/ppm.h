#ifndef PPM_H
#define PPM_H

typedef struct {
    int width;
    int height;
    unsigned char *data;
} PPMImage;

PPMImage *read_ppm(const char *filename);
int write_ppm(const char *filename, PPMImage *image);
void free_ppm(PPMImage *image);

#endif // PPM_H
