
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ppm.h"
#include "bit_utils.h"
#include "steganography.h"

int encode(const char *input_ppm, const char *payload, const char *output_ppm) {
    PPMImage *image = read_ppm(input_ppm);
    if (!image) return 1;

    FILE *payload_file = fopen(payload, "rb");
    if (!payload_file) {
        perror("Error opening payload file");
        free_ppm(image);
        return 1;
    }

    fseek(payload_file, 0, SEEK_END);
    long payload_size = ftell(payload_file);
    fseek(payload_file, 0, SEEK_SET);

    if (payload_size > (image->width * image->height * 3 - 16) / 8) {
        fprintf(stderr, "Error: Payload too large to encode\n");
        free_ppm(image);
        fclose(payload_file);
        return 1;
    }

    unsigned char *payload_data = malloc(payload_size);
    if (!payload_data) {
        perror("Error allocating memory");
        free_ppm(image);
        fclose(payload_file);
        return 1;
    }

    fread(payload_data, 1, payload_size, payload_file);
    fclose(payload_file);

    unsigned char *data_ptr = image->data;
    for (int i = 0; i < 16; i++) {
        unsigned char bit = (payload_size >> (15 - i)) & 1;
        if (bit != (get_bit(data_ptr[1], 0) ^ get_bit(data_ptr[2], 0))) {
            data_ptr[2] = toggle_bit(data_ptr[2], 0);
        }
        data_ptr += 3;
    }

    for (long i = 0; i < payload_size * 8; i++) {
        unsigned char bit = (payload_data[i / 8] >> (7 - (i % 8))) & 1;
        if (bit != (get_bit(data_ptr[1], 0) ^ get_bit(data_ptr[2], 0))) {
            data_ptr[2] = toggle_bit(data_ptr[2], 0);
        }
        data_ptr += 3;
    }

    free(payload_data);

    if (write_ppm(output_ppm, image)) {
        free_ppm(image);
        return 1;
    }

    free_ppm(image);
    return 0;
}

int decode(const char *input_ppm, const char *output_file) {
    PPMImage *image = read_ppm(input_ppm);
    if (!image) return 1;

    unsigned long payload_size = 0;
    unsigned char *data_ptr = image->data;

    for (int i = 0; i < 16; i++) {
        unsigned char bit = get_bit(data_ptr[1], 0) ^ get_bit(data_ptr[2], 0);
        payload_size = (payload_size << 1) | bit;
        data_ptr += 3;
    }

    unsigned char *payload_data = malloc(payload_size);
    if (!payload_data) {
        perror("Error allocating memory");
        free_ppm(image);
        return 1;
    }

    for (unsigned long i = 0; i < payload_size * 8; i++) {
        unsigned char bit = get_bit(data_ptr[1], 0) ^ get_bit(data_ptr[2], 0);
        payload_data[i / 8] = (payload_data[i / 8] << 1) | bit;
        data_ptr += 3;
    }

    FILE *output_fp = fopen(output_file, "wb");
    if (!output_fp) {
        perror("Error opening output file");
        free(payload_data);
        free_ppm(image);
        return 1;
    }

    fwrite(payload_data, 1, payload_size, output_fp);
    fclose(output_fp);

    free(payload_data);
    free_ppm(image);
    return 0;
}
