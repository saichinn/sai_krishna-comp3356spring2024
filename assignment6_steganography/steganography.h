// steganography.h
#ifndef STEGANOGRAPHY_H
#define STEGANOGRAPHY_H

int encode(const char *input_ppm, const char *payload, const char *output_ppm);
int decode(const char *input_ppm, const char *output_file);

#endif // STEGANOGRAPHY_H
